# code-verifier-backend
Proyecto Back-End: Express, Node, Typescript, Jest, ESLint, 

## Dependencias Usadas en el Proyecto
Dependencies
dotenv: se utilizara para tener a nivel de proyecto un archivo de entorno .env
express: framework que se utiliza para crear una aplicacion de Back-End con Node

devDependencies
@types/express: Express para Typescript
@types/jest: definiciones Typescript para jest
@types/node: Node para Typescript
@typescript-eslint/eslint-plugin: ESLint para Typescript
concurrently: Ejecuta multiples commandos al mismo tiempo
eslint: Para escritura de codigo ordenada
jest: Para test unitarios
nodemon: Utilidad de interfaz de línea de comandos, que envuelve su aplicación Node, vigila el sistema de archivos y reinicia automáticamente el proceso, ante cualquier modificacion.
serve: Para servir nuestro coverage a nivel de web, y poder visualizarlo
supertest: Libreria para testiar servidores HTTP
ts-jest: Jest mapeado para usarlo en Typescript
ts-node: Node para Typescript
typescript: Instalando Typescript
webpack: Esto nos permitira empaquetar mucho la solucion (index.js), creando una solucion mucho mas ligera.
webpack-cli: esto nos permitira ejecutar webpack
webpack-node-externals: permite que ignore node_modules, al empaquetar.
webpack-shell-plugin: Servira para luego hacer la configuracion de webpack

## Scripts
build: De el archivo TS, transpila el JS
start: Ejecuta el archivo transpilado.
dev: Ve los cambios que se producen en el TS y transpila un nuevo archivo, ejecuta el archivo transpilado con nodemon, modifica los cambios instantaneamente.
test: Para ejecutar los test en Jest.
serve:coverage: Ejecuta los test, va a la ruta de los reportes, sirve los reportes en el puerto 3000

## Variables de entorno en archivo .env necesarias
PORT=8000: Donde correra nuestra app
