"use strict";
//index.ts
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//Solicitando Express
//import, importando libreria express
//VSCode, ctrl + espacio, para buscar modulos
//Export default express, que sera toda la libreria por defecto (alias)
const express_1 = __importDefault(require("express"));
//Solicitando dotenv
//import, importando libreria dotenv
//Export default dotenv, que sera toda la libreria por defecto (alias)
const dotenv_1 = __importDefault(require("dotenv"));
//Configuration the .env file
//Esto sirve para poder hacer uso del archivo .env
//Archivo oculto
dotenv_1.default.config();
//Creando la APP Express
//app, seria el server, que guarda una ejecucion de express
//Typescript es un lenguaje tipado
//:, las variables se tipean, : Express vendria siendo una interfaz
// al tipearlo, la variable app, podra acceder a todas las funcionalidades de Express
const app = (0, express_1.default)();
//Creando el puerto de ejecucion
//Para llamar a .env, se antepone process siempre
//Se llama al puerto creado en .env o || se da manualmente si no lo encuentra
const port = process.env.PORT || 8000;
//Definiendo la primera ruta de la APP
// '/', el proyecto se despliega en local, en el puerto creado (localhost:8000/)
//Los parametros de express que recibe son:
//Request, requerimiento
//Response, respuesta de la peticion
app.get('/', (req, res) => {
    //Enviar un hola Mundo
    //Response, enviando un body al cliente, que seria el navegador
    res.send('Welcome to API Restful: Express + TS + Nodemon + Jest + Swagger + Mongoose');
});
//Definiendo otra ruta 
app.get('/hello', (req, res) => {
    //Enviar un hola Mundo
    res.send('Welcome to GET Route: Hello!');
});
//Ejecuntado la APP y llamar(listen) la respuesta al Puerto(port)
app.listen(port, () => {
    console.log(`EXPRESS SERVER: Running at http://localhost:${port}`);
});
//# sourceMappingURL=index.js.map